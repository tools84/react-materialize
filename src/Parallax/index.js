import React, { useEffect } from 'react';
import './parallax.css';

const Parallax = ({ image }) => {
	const elRef = React.createRef();

	useEffect(() => {
		const options = {};
		const instance = M.Parallax.init(elRef.current, options);
		return () => {
			instance && instance.destroy();
		};
	});

	return (
		<div ref={elRef} className="parallax-container">
			<div className="parallax">
				<img className="" alt="parallax" src={image} />
			</div>
		</div>
	);
};

Parallax.defaultProps = {
	image: ''
};
export default Parallax;
