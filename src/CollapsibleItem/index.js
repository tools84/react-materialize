import React from "react";

export const CollapsibleItem = ({ title, icon = false, iconComponent=false, editIcon = '', active='', extraClass = '', style={}, children  }) => {
    const CustomIcon = iconComponent;
    return (
        <li className={`${extraClass} ${active} `} style={style}>
            <div className={`collapsible-header`}>
                {icon && <i className={icon} />}
                {editIcon}
                {title}
                {CustomIcon && <CustomIcon/>}
            </div>
            <div className="collapsible-body">
                <span>{children}</span>
            </div>
        </li>
    );
};

CollapsibleItem.defaultProps = {
    title: '',
    icon: false,
    active: false
};

export default CollapsibleItem;
