import React, { useEffect } from 'react';
import './style.css';

/**
 *<Dropdown id='' label='' options={[]} />
 * @param options =[ {title:'', url:''}, ...]
 * @returns {*}
 */
const Dropdown = ({ id, label, options, activeComponent }) => {
	const myRef = React.createRef();
	let instance = null;

	useEffect(() => {
		const conf = { coverTrigger: false,
			alignment: 'right',
			constrainWidth: false };
		instance =  M.Dropdown.init(myRef.current, conf);
	},[]);

	return (
		<div className="drpodownMenu">
            <label>{label}</label>
			<TriggerIcon id={id} CustomElement={activeComponent} myRef={myRef} />

			{/* Links */}
			<ul id={`dropdown${id}`} className="dropdown-content">
				{options && options.map((item, i) => {
						return (
							<li key={i}>
								<a href={item.url}>
									{item.icon && <i className="material-icons">{item.icon}</i>}
									{item.title}
								</a>
							</li>
						);
					})}
			</ul>
		</div>
	);
};

Dropdown.defaultProps = {
	id: 1,
    label: '',
	options: []
};

const TriggerIcon = ({ id, CustomElement, myRef }) => {
	return (
		<a ref={myRef} className="dropdown-trigger" data-target={`dropdown${id}`}>
			<div className="nav-avatar-dropdown-trigger">{CustomElement || <i className="material-icons">arrow_drop_down</i>}</div>
		</a>
	);
};

export default Dropdown;
