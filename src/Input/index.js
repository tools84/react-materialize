import React from 'react';

class Input extends React.Component {
	static defaultProps = {
		id: 1,
		status: 'active',
		type: 'text',
		value: 'Peter Hong',
		placeholder: 'Peter Hong',
		className: 'validate'
	};

	componentDidUpdate(prevProps, prevState) {
		M.updateTextFields();
	}

	render() {
		const { id = '', label = '', value = '', placeholder, onChange } = this.props;

		return (
			<div className="input-field">
				<input id={id} status="active" placeholder={placeholder} type="text" value={value} className="validate" onChange={e => (onChange ? onChange(e.target.value) : false)} />
				<label >{label}</label>
			</div>
		);
	}
}
export default Input;
