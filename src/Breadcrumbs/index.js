import React from 'react';
import './styles.css';

/**
 * @param links = {link, title}
 * @returns {*}
 */
const Breadcrumbs = props => {
	const { links } = props;

	return (
		<div className="breadcrumbs">
			<a className="breadcrumb" href="/">
				<i className="material-icons">home</i>
			</a>
			{links &&
				links.map((el, i) => (
					<a key={i} href={el.link} className="breadcrumb ">
						{el.title}
					</a>
				))}
		</div>
	);
};
Breadcrumbs.defaultProps = {
	links: []
};
export default Breadcrumbs;
