import React, { Component, Fragment, useEffect } from 'react';
import './Tabs.css';


const Tabs = ({ id,  menuList = [], extraClass = '', menuColor, style = {}, tabChangedCallback , children }) => {
	const elRef = React.useRef();

	useEffect(() => {
		const instance = M.Tabs.init(elRef.current, {});
		return () => {
			instance && instance.destroy();
		};
	});

	const menuStyle = {};
	if (menuColor) menuStyle.backgroundColor = menuColor;

	return (
		<div className={`row tabs-${id} ${extraClass} `} style={style}>
			d
			<div className={`col s12 tab-menu`} style={menuStyle}>
				<ul id={id} ref={elRef} className="tabs tabs-fixed-width">
					{menuList.length > 0 && menuList.map((item, i) => {
							return (
								<li key={i} className="tab" style={menuStyle} onClick={tabChangedCallback}>
									<a id={i} className={i === 0 ? 'active' : ''} href={`#tab-${id}-${i + 1}`}>
										{item}
									</a>
								</li>
							);
						})}
				</ul>
			</div>
			{children}
		</div>
	);
};

Tabs.defaultProps = {
	id:1,
	menuList: ['']
};

export default Tabs;
