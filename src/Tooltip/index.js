import React, { useEffect } from 'react';


const Tooltip = ({ icon, color, tooltipData, onClick }) => {
	const elRef = React.useRef();
	const options = { position: 'top' };

	useEffect(() => {
		const instance = M.Tooltip.init(elRef.current, options);
		return () => {
			instance && instance.destroy();
		};
	});

	return (
		<a ref={elRef} onClick={onClick} data-tooltip={tooltipData}>
			<i className={`material-icons ${color}`}>{icon}</i>
		</a>
	);
};

Tooltip.defaultProps = {
	tooltipData: {},
	icon: ''
};

export default Tooltip;
