import React, { Fragment, Component, useState, useEffect } from 'react';


const Collapsible = ({ children, extraClass ='' }) => {
	const ref = React.createRef();
	let options = {};

	useEffect(() => {
		const instance = M.Collapsible.init(ref.current, {});
		return () => {
			instance && instance.destroy();
		};
	});

	return (
		<ul ref={ref} className={`collapsible ${extraClass}`}>
			{children}
		</ul>
	);
};

Collapsible.defaultProps = {
	extraClass: false
};

export default Collapsible;

// export const CollapsibleItem = ({ title, icon = false, editIcon = '', active, extraClass = '', children }) => {
// 	return (
// 		<li className={`${extraClass} ${active ? 'active' : ''}`}>
// 			<div className={`collapsible-header  `}>
// 				{icon && <i className={icon} />}
// 				{editIcon}
// 				{title}
// 			</div>
// 			<div className="collapsible-body">
// 				<span>{children}</span>
// 			</div>
// 		</li>
// 	);
// };
//
// CollapsibleItem.defaultProps = {
// 	title: '',
// 	icon: false,
// 	active: false
// };
