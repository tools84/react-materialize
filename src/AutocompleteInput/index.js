import React, { useState, useRef, useEffect } from 'react';

const Autocomplete = ({ data, icon, placeholder, defaultValue, onChangeCallback }) => {
	const [inputValue, setInputValue] = useState(defaultValue || '');
	const elRef = useRef();

	useEffect(
		() => {
			const _options={
				data,
				onAutocomplete: val => {
					setInputValue(val);
				}
			};
			const instance = M.Autocomplete.init(elRef.current, _options);
			return () => {
				instance && instance.destroy();
			};
		},
		[data]
	);

	const onChangeInput = e => {
		if(onChangeCallback) onChangeCallback(e.target.value);
	};

	return (
		<div className="input-field">
			<i className="material-icons prefix">{icon}</i>
			<input id="autocomplete-input" type="text" ref={elRef} onChange={onChangeInput} placeholder={placeholder} defaultValue={inputValue} className="autocomplete" autoComplete="off" />
		</div>
	);
};

Autocomplete.defaultProps = {
	id: 1,
	placeholder: '',
	data: {},
	onChangeCallback: false
};

export default Autocomplete;
