export default function Row({sm, md, lg, xl, children}){
    const cl=['row'];
    if(sm) cl.push(`s${sm}`);
    if(md) cl.push(`m${md}`);
    if(lg) cl.push(`l${lg}`);
    if(xl) cl.push(`xl${xl}`);

    return(<div className={cl.join(' ')}>{children}</div>)
}
