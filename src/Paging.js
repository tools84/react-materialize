import React, { useState } from 'react';

/**
 * @onClick
 * @total
 * @pageNo
 * @limit
 */

const Paging = ({ total, pageNo, limit, onClick }) => {
	const [currentPage, setCurrentPage] = useState(pageNo || 1);
	const pageSize = limit;
	const totalPages = total <= limit ? 0 : Math.ceil(Number(total) / pageSize);

	const getPagesArr = (startPage, endPage) => {
		let res = [];
		for (let i = startPage; i <= endPage; i++) {
			res.push(i);
		}
		return res;
	};

	const getStartEnd = (totalPages, currentPage) => {
		let startPage, endPage;

		if (totalPages <= 10) {
			// less than 10 total pages so show all
			startPage = 1;
			endPage = totalPages;
		} else {
			// more than 10 total pages so calculate start and end pages
			if (currentPage <= 6) {
				startPage = 1;
				endPage = 10;
			} else if (currentPage + 4 >= totalPages) {
				startPage = totalPages - 9;
				endPage = totalPages;
			} else {
				startPage = currentPage - 5;
				endPage = currentPage + 4;
			}
		}

		return { startPage, endPage };
	};

	const onPageClick = (e, pageNumber) => {
        setCurrentPage(pageNumber);
		if (onClick) onClick(e, pageNumber);
	};

	let { startPage, endPage } = getStartEnd(totalPages, currentPage);
	let pagesArr = getPagesArr(startPage, endPage);

	let arrowStyleFirst = currentPage == 1 || currentPage < pageSize ? 'disabled' : 'waves-effect';
	let arrowStyleLast = currentPage == totalPages || currentPage > totalPages - pageSize ? 'disabled' : 'waves-effect';

	if (totalPages <= 0) {
		return false;
	} else {
		return (
			<ul className="pagination">
				<li onClick={e => onPageClick(e, 1)} className={arrowStyleFirst}>
					<a>
						<i className="material-icons">chevron_left</i>
					</a>
				</li>

				{pagesArr.map((page, i) => {
					let istyle = page == currentPage ? 'active' : 'waves-effect';

					return (
						<li key={i} onClick={e => onPageClick(e, page)} className={istyle}>
							<a>{page}</a>
						</li>
					);
				})}

				<li onClick={e => onPageClick(e, totalPages)} className={arrowStyleLast}>
					<a>
						<i className="material-icons">chevron_right</i>
					</a>
				</li>
			</ul>
		);
	}
};

Paging.defaultProps = {
	pageNo: 0,
	total: 0,
	limit: 5
};

export default Paging;
