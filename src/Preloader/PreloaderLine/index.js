import React  from 'react';
import './styles.css';

const PreloaderLine = (props)=>{
    const {style={}} =props;

    return(<div className="progress" style={style}>
        <div className="indeterminate"></div>
    </div>);
};

export default PreloaderLine;
