import React from "react";

const PreloaderSpinner = (props) => {
    const {size, color, extraStyles} = props;
    const cStyles = extraStyles || {};
    cStyles.verticalAlign = 'middle';
    if(size && size =='extra-big') {
        cStyles.width = '80px';
        cStyles.height = '80px';
    //     cStyles.verticalAlign = size;
    }


    return (
        <div className={`preloader-wrapper ${size} active`} style={cStyles}>
            <div className="spinner-layer spinner-blue-only">
                <div className="circle-clipper left">
                    <div className="circle" />
                </div>
                <div className="gap-patch">
                    <div className="circle" />
                </div>
                <div className="circle-clipper right">
                    <div className="circle" />
                </div>
            </div>
        </div>
    );
};
export default PreloaderSpinner;
