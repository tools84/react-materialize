import React from 'react';
import Paging from '../src/Paging';
// import '../../src/ActionBtn/actionStyles.css';


export default {
    title: 'Other/Paging',
    component: Paging,
    argTypes: {},
};

const props={
    total:30,
    limit:5,
    pageNo:1
};

const Template = (args) => <Paging {...props} />;
export const Primary = Template.bind({});

