import React from 'react';
import DatePicker from '../src/DatePicker';

export default {
	title: 'Forms/DatePicker',
	component: DatePicker,
	argTypes: {}
};

const param = {};
const Template = args => <DatePicker {...param} />;
export const Primary = Template.bind({});
