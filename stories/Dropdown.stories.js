import React from 'react';
import Dropdown from '../src/Dropdown';
// import '../../src/ActionBtn/actionStyles.css';


export default {
    title: 'Other/Dropdown',
    component: Dropdown,
    argTypes: {},
};


const param={
    label:'User Profile',
    links:[ {title:'Account', url:'/'}, {title:'Log Out', url:'/'}]
};
const Template = (args) => <Dropdown {...args} />;
export const Primary = Template.bind({});
Primary.args={
    label:'User Menu',
    links:[ {title:'Account', url:'/'}, {title:'Log Out', url:'/'}]
};
